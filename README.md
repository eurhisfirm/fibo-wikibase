# FIBO-Wikibase

_Wikibase bots to import the Financial Industry Business Ontology (FIBO)_

## Install

```bash
git clone https://gitlab.huma-num.fr/eurhisfirm/fibo-wikibase.git
cd fibo-wikibase/
```

Edit `src/config.js` to change database informations. Then

```bash
npm install
```

## Download and convert FIBO vocabulary to JSON

```bash
mkdir ../fibo-data
npx babel-node src/scripts/fibo_to_quads.js ../fibo-data/
```

## Update Wikibase with FIBO vocabulary

```bash
npx babel-node src/scripts/quads_to_wikibase.js  ../fibo-data/
```

## Maintenance

### Delete all entities from Wikibase

**Caution**: This removes all Wikibase items & properties from SQL database, even those that are not FIBO related!

As `root` user:

```bash
mysql eurhisfirm
```

```sql
delete from wb_terms;
delete from wb_changes;
delete from wb_property_info;
delete from wb_items_per_site;
delete from wb_id_counters;
delete from page where page_content_model = 'wikibase-item';
delete from page where page_content_model = 'wikibase-property';
exit
```

```bash
systemctl restart apache-htcacheclean.service
systemctl restart apache2.service
```
