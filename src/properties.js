import assert from "assert"

let equivalentItemPropertyId = null
let equivalentPropertyPropertyId = null
let retrievedPropertyId = null
let statedInPropertyId = null

export async function getEquivalentItemPropertyId(wikibase) {
  if (equivalentItemPropertyId === null) {
    const property = await wikibase.upsertPropertyCore({
      datatype: "url",
      descriptions: [
        {
          language: "en",
          value:
            "equivalent item in other ontologies (use in statements on items, use item URI)",
        },
        {
          language: "fr",
          value:
            "élément équivalent dans d'autres ontologies (usage dans les déclarations des éléments," +
            " utiliser l'URI de l'élément)",
        },
      ],
      labels: [
        {
          language: "en",
          value: "equivalent item",
        },
        {
          language: "fr",
          value: "élément équivalent",
        },
      ],
    })
    equivalentItemPropertyId = property.id
    console.log("equivalent item property: ", equivalentItemPropertyId)

    const claims = property.claims
    assert(claims !== undefined, JSON.stringify(property, null, 2))
    let lastrevid = property.lastrevid
    assert(lastrevid !== undefined, JSON.stringify(property, null, 2))
  }

  return equivalentPropertyPropertyId
}

export async function getEquivalentPropertyPropertyId(wikibase) {
  if (equivalentPropertyPropertyId === null) {
    const property = await wikibase.upsertPropertyCore({
      datatype: "url",
      descriptions: [
        {
          language: "en",
          value:
            "equivalent property in other ontologies (use in statements on properties, use property URI)",
        },
        {
          language: "fr",
          value:
            "propriété équivalente dans d'autres ontologies (usage dans les déclarations dans les propriétés," +
            " utiliser l'URI de la propriété)",
        },
      ],
      labels: [
        {
          language: "en",
          value: "equivalent property (url)",
        },
        {
          language: "fr",
          value: "propriété équivalente (url)",
        },
      ],
    })
    equivalentPropertyPropertyId = property.id
    console.log("equivalent property property: ", equivalentPropertyPropertyId)

    const claims = property.claims
    assert(claims !== undefined, JSON.stringify(property, null, 2))
    let lastrevid = property.lastrevid
    assert(lastrevid !== undefined, JSON.stringify(property, null, 2))

    // This property is itself an equivalent property of Wikidata "equivalent property" P1628.
    const existingWikidataEquivalentPropertyClaims = (
      claims[equivalentPropertyPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datatype === "url" &&
        claim.mainsnak.datavalue.type === "string" &&
        claim.mainsnak.datavalue.value ===
          "https://www.wikidata.org/wiki/Property:P1628",
    )
    if (existingWikidataEquivalentPropertyClaims.length === 0) {
      let result = await wikibase.postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: property.id,
        format: "json",
        property: equivalentPropertyPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify("https://www.wikidata.org/wiki/Property:P1628"),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      assert(result.claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return equivalentPropertyPropertyId
}

export async function getRetrievedPropertyId(wikibase) {
  if (retrievedPropertyId === null) {
    const property = await wikibase.upsertPropertyCore({
      datatype: "time",
      descriptions: [
        {
          language: "en",
          value:
            "date or point in time that information was retrieved from a database or website" +
            " (for use in online sources)",
        },
        {
          language: "fr",
          value:
            "date à laquelle l'information a été importée d'une base de données ou consultée sur un site web",
        },
      ],
      labels: [
        {
          language: "en",
          value: "retrieved",
        },
        {
          language: "fr",
          value: "date de consultation",
        },
      ],
    })
    retrievedPropertyId = property.id
    console.log("retrieved property: ", retrievedPropertyId)

    const claims = property.claims
    assert(claims !== undefined, JSON.stringify(property, null, 2))
    let lastrevid = property.lastrevid
    assert(lastrevid !== undefined, JSON.stringify(property, null, 2))

    // This property is an equivalent property of Wikidata "retrieved" P813.
    const equivalentPropertyPropertyId = await getEquivalentPropertyPropertyId(
      wikibase,
    )
    const existingWikidataStatedInClaims = (
      claims[equivalentPropertyPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datatype === "url" &&
        claim.mainsnak.datavalue.type === "string" &&
        claim.mainsnak.datavalue.value ===
          "https://www.wikidata.org/wiki/Property:P813",
    )
    if (existingWikidataStatedInClaims.length === 0) {
      let result = await wikibase.postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: property.id,
        format: "json",
        property: equivalentPropertyPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify("https://www.wikidata.org/wiki/Property:P813"),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      assert(result.claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return retrievedPropertyId
}

export async function getStatedInPropertyId(wikibase) {
  if (statedInPropertyId === null) {
    const property = await wikibase.upsertPropertyCore({
      datatype: "wikibase-item",
      descriptions: [
        {
          language: "en",
          value:
            "to be used in the references field to refer to the information document or database in which" +
            " a claim is made",
        },
        {
          language: "fr",
          value:
            "à utiliser dans le champ références, pour indiquer le document source dans lequel la déclaration" +
            " est corroborée",
        },
      ],
      labels: [
        {
          language: "en",
          value: "stated in",
        },
        {
          language: "fr",
          value: "affirmé dans",
        },
      ],
    })
    statedInPropertyId = property.id
    console.log("stated in property: ", statedInPropertyId)

    const claims = property.claims
    assert(claims !== undefined, JSON.stringify(property, null, 2))
    let lastrevid = property.lastrevid
    assert(lastrevid !== undefined, JSON.stringify(property, null, 2))

    // This property is an equivalent property of Wikidata "stated in" P248.
    const equivalentPropertyPropertyId = await getEquivalentPropertyPropertyId(
      wikibase,
    )
    const existingWikidataStatedInClaims = (
      claims[equivalentPropertyPropertyId] || []
    ).filter(
      claim =>
        claim.mainsnak.snaktype === "value" &&
        claim.mainsnak.datatype === "url" &&
        claim.mainsnak.datavalue.type === "string" &&
        claim.mainsnak.datavalue.value ===
          "https://www.wikidata.org/wiki/Property:P248",
    )
    if (existingWikidataStatedInClaims.length === 0) {
      let result = await wikibase.postToApi({
        action: "wbcreateclaim",
        baserevid: lastrevid,
        bot: true,
        entity: property.id,
        format: "json",
        property: equivalentPropertyPropertyId,
        snaktype: "value",
        // summary: "Adding claim",
        value: JSON.stringify("https://www.wikidata.org/wiki/Property:P248"),
      })
      lastrevid = result.pageinfo.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
      assert(result.claim !== undefined, JSON.stringify(result, null, 2))
    }
  }

  return statedInPropertyId
}
