let falseItemId = null
let fiboItemId = null
let trueItemId = null

export async function getFalseItemId(wikibase) {
  if (falseItemId === null) {
    falseItemId = (await wikibase.upsertItemCore({
      descriptions: [
        {
          language: "en",
          value: "boolean value",
        },
        {
          language: "fr",
          value: "valeur booléenne",
        },
      ],
      labels: [
        {
          language: "en",
          value: "false",
        },
        {
          language: "fr",
          value: "faux",
        },
      ],
    })).id
  }
  return falseItemId
}

export async function getFiboItemId(wikibase) {
  if (fiboItemId === null) {
    fiboItemId = (await wikibase.upsertItemCore({
      aliases: [
        {
          language: "en",
          value: "FIBO",
        },
        {
          language: "fr",
          value: "FIBO",
        },
      ],
      descriptions: [
        {
          language: "en",
          value:
            "definitions of business concepts in the financial services industry",
        },
        {
          language: "fr",
          value:
            "définitions des concepts métiers dans l'industrie des services financiers",
        },
      ],
      labels: [
        {
          language: "en",
          value: "Financial Industry Business Ontology",
        },
        {
          language: "fr",
          value: "Financial Industry Business Ontology",
        },
      ],
    })).id
  }
  return fiboItemId
}

export async function getTrueItemId(wikibase) {
  if (trueItemId === null) {
    trueItemId = (await wikibase.upsertItemCore({
      descriptions: [
        {
          language: "en",
          value: "boolean value",
        },
        {
          language: "fr",
          value: "valeur booléenne",
        },
      ],
      labels: [
        {
          language: "en",
          value: "true",
        },
        {
          language: "fr",
          value: "vrai",
        },
      ],
    })).id
  }
  return trueItemId
}
