import { assertValid } from "@biryani/core"
import assert from "assert"
import commandLineArgs from "command-line-args"
import deepEqual from "fast-deep-equal"
import fs from "fs"
import path from "path"
import rpn from "request-promise-native"

import pkg from "../../package.json"
import config from "../config"
import { cleanUpLine, numericIdFromItemId, truncate } from "../helpers"
import { getFalseItemId, getFiboItemId, getTrueItemId } from "../items"
import {
  getEquivalentPropertyPropertyId,
  getRetrievedPropertyId,
  getStatedInPropertyId,
} from "../properties"
import {
  assertValid,
  validateQuad,
  validateSubjectValue,
} from "../validators/quads"
import Wikibase from "../wikibase"

const optionDefinitions = [
  {
    name: "dir",
    type: String,
    defaultOption: true,
    help: "directory where to store FIBO quads",
  },
]
const options = commandLineArgs(optionDefinitions)
const predicatesUrls = new Set()
const propertyIdByDataTypeByPredicateUrl = {}
const reservedEntitiesNames = new Set(["retrieved", "stated in"])
let validQuadsBySubjectValue = null

function dataTypeFromObject(object) {
  const dataTypeUrl =
    object.datatype === undefined ? null : object.datatype.value
  let dataType =
    dataTypeUrl === null
      ? // {
        //   "http://purl.org/dc/terms/hasPart": "url",
        //   "https://spec.edmcouncil.org/fibo/ontology/FND/Relations/Relations/hasIdentity": "url",
        //   "https://spec.edmcouncil.org/fibo/ontology/FND/Utilities/AnnotationVocabulary/hasMaturityLevel": "url",
        //   "http://www.w3.org/1999/02/22-rdf-syntax-ns#first": "url",
        //   "http://www.w3.org/1999/02/22-rdf-syntax-ns#rest": "url",
        //   "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "url",
        //   "http://www.w3.org/2000/01/rdf-schema#domain": "url",
        //   "http://www.w3.org/2000/01/rdf-schema#isDefinedBy": "url",
        //   "http://www.w3.org/2000/01/rdf-schema#range": "url",
        //   "http://www.w3.org/2000/01/rdf-schema#subClassOf": "url",
        //   "http://www.w3.org/2000/01/rdf-schema#subPropertyOf": "url",
        //   "http://www.w3.org/2002/07/owl#allValuesFrom": "url",
        //   "http://www.w3.org/2002/07/owl#disjointWith": "url",
        //   "http://www.w3.org/2002/07/owl#equivalentClass": "url",
        //   "http://www.w3.org/2002/07/owl#equivalentProperty": "url",
        //   "http://www.w3.org/2002/07/owl#intersectionOf": "url",
        //   "http://www.w3.org/2002/07/owl#inverseOf": "url",
        //   "http://www.w3.org/2002/07/owl#onClass": "url",
        //   "http://www.w3.org/2002/07/owl#onDataRange": "url",
        //   "http://www.w3.org/2002/07/owl#onProperty": "url",
        //   "http://www.w3.org/2002/07/owl#propertyChainAxiom": "url",
        //   "http://www.w3.org/2002/07/owl#someValuesFrom": "url",
        //   "http://www.w3.org/2002/07/owl#unionOf": "url",
        //   "http://www.w3.org/2002/07/owl#versionIRI": "url",
        // }[predicateUrl]
        "url"
      : {
          "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString":
            "monolingualtext",
          // Cf https://www.wikidata.org/wiki/Help:Data_type#Boolean: Declined. Suggested alternative: item-datatype.
          "http://www.w3.org/2001/XMLSchema#boolean": "boolean",
          "http://www.w3.org/2001/XMLSchema#dateTime": "time",
          "http://www.w3.org/2001/XMLSchema#decimal": "quantity",
          "http://www.w3.org/2001/XMLSchema#integer": "quantity",
          "http://www.w3.org/2001/XMLSchema#nonNegativeInteger": "quantity",
          "http://www.w3.org/2001/XMLSchema#anyURI": "url",
          "http://www.w3.org/2001/XMLSchema#string": "string",
        }[dataTypeUrl]
  if (dataType === "url") {
    const url = object.value
    if (validQuadsBySubjectValue[url]) {
      // This is the URL of a subject.
      dataType = predicatesUrls.has(url) ? "wikibase-property" : "wikibase-item"
    }
  }
  return dataType
}

function last2SegmentsFromUrl(url) {
  if (/^b\d+$/.test(url)) {
    return [null, url]
  }
  let remainingUrl = url.replace(/#$/, "")
  const sharpIndex = remainingUrl.indexOf("#")
  let segment1 = null
  if (sharpIndex < 0) {
    remainingUrl = remainingUrl.replace(/\/$/, "")
    const slashIndex = remainingUrl.lastIndexOf("/")
    assert.notStrictEqual(slashIndex, -1)
    segment1 = remainingUrl.substring(slashIndex + 1)
    remainingUrl = remainingUrl.substring(0, slashIndex)
  } else {
    segment1 = remainingUrl.substring(sharpIndex + 1)
    remainingUrl = remainingUrl.substring(0, sharpIndex)
  }
  assert(segment1, `Unable to extract last segment from URL ${url}`)
  remainingUrl = remainingUrl.replace(/\/$/, "")
  const slashIndex = remainingUrl.lastIndexOf("/")
  assert.notStrictEqual(slashIndex, -1)
  const segment0 = remainingUrl.substring(slashIndex + 1)
  assert(segment0, `Unable to extract previous segment from URL ${url}`)
  return [segment0, segment1]
}

async function main() {
  assert(fs.statSync(options.dir).isDirectory())
  const quadsFilePath = path.join(options.dir, "fibo_quads.json")
  const quadsModificationTime = fs.statSync(quadsFilePath).mtime
  const quadsBySubjectValue = JSON.parse(fs.readFileSync(quadsFilePath, "utf8"))

  // Validate and repair quads.
  validQuadsBySubjectValue = Object.entries(quadsBySubjectValue).reduce(
    (validQuadsBySubjectValue, [subjectValue, quads]) => {
      const validQuads = quads
        .map(quad => assertValid(validateQuad(quad)))
        .filter(
          (quad, index, validQuads) =>
            !validQuads
              .slice(0, index)
              .some(previousQuad => deepEqual(previousQuad, quad)),
        )
      const validSubjectValue = assertValid(validateSubjectValue(subjectValue))
      validQuadsBySubjectValue[validSubjectValue] = validQuads
      return validQuadsBySubjectValue
    },
    {},
  )

  // const rangeObjectsValues = new Set()
  // for (let quads of Object.values(validQuadsBySubjectValue)) {
  //   if (quadsHaveIriObject(quads, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "http://www.w3.org/2002/07/owl#DatatypeProperty")) {
  //     for (let quad of quads) {
  //       if (quad.predicate.value === "http://www.w3.org/2000/01/rdf-schema#range") {
  //         rangeObjectsValues.add(quad.object.value)
  //       }
  //     }
  //   }
  // }
  // console.log(JSON.stringify([...rangeObjectsValues], null, 2))

  // Determine the data type of each subject.
  const dataTypeBySubjectValue = {}
  for (let [subjectValue, quads] of Object.entries(validQuadsBySubjectValue)) {
    let dataType = null
    const typeObjects = quadsObjects(
      quads,
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
    )
    if (typeObjects.length === 0) {
      if (
        quadsHavePredicate(
          quads,
          "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/isSubregionOf",
        )
      ) {
        // Subregion
        dataType = "wikibase-item"
      } else if (
        quadsHavePredicate(
          quads,
          "http://www.w3.org/1999/02/22-rdf-syntax-ns#first",
        )
      ) {
        // List without type "http://www.w3.org/1999/02/22-rdf-syntax-ns#List"
        dataType = "wikibase-item"
      } else if (
        quadsHavePredicate(
          quads,
          "http://www.w3.org/2001/XMLSchema#maxInclusive",
        )
      ) {
        // Max Inclusive number value
        dataType = "wikibase-item"
      } else if (
        quadsHavePredicate(
          quads,
          "http://www.w3.org/2001/XMLSchema#minInclusive",
        )
      ) {
        // Min Inclusive number value
        dataType = "wikibase-item"
      } else {
        throw `Definition of ${subjectValue} has no type:\n${JSON.stringify(
          quads,
          null,
          2,
        )}`
      }
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/CountrySubdivision",
      )
    ) {
      // Country Subdivision
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/GeographicRegion",
      )
    ) {
      // Geographic Region
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/GeographicRegionIdentifier",
      )
    ) {
      // Geographic Region Identifier
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/GeographicRegionKind",
      )
    ) {
      // Geographic Region Kind
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/Territory",
      )
    ) {
      // Territory
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Languages/LanguageRepresentation/LanguageFamily",
      )
    ) {
      // Language Family
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/spec/LCC/Languages/LanguageRepresentation/LivingLanguage",
      )
    ) {
      // Living Language
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.omg.org/techprocess/ab/SpecificationMetadata/Module",
      )
    ) {
      // Module
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#List",
      )
    ) {
      // List
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/2000/01/rdf-schema#Datatype",
      )
    ) {
      // Datatype
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(typeObjects, "http://www.w3.org/2002/07/owl#AllDifferent")
    ) {
      // All Different
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/2002/07/owl#AnnotationProperty",
      )
    ) {
      // Annotation Property
      dataType = "wikibase-property"
    } else if (
      objectsHaveIri(typeObjects, "http://www.w3.org/2002/07/owl#Class")
    ) {
      // Class
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/2002/07/owl#DatatypeProperty",
      )
    ) {
      // Datatype Property
      dataType = "wikibase-property"
    } else if (
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/2002/07/owl#ObjectProperty",
      )
    ) {
      // Object Property
      dataType = "wikibase-property"
    } else if (
      objectsHaveIri(typeObjects, "http://www.w3.org/2002/07/owl#Ontology")
    ) {
      // Ontology
      dataType = "wikibase-item"
    } else if (
      objectsHaveIri(typeObjects, "http://www.w3.org/2002/07/owl#Restriction")
    ) {
      // Restriction (on a property)
      dataType = "wikibase-item"
    } else if (
      typeObjects.some(({ value }) => {
        const typeQuads = validQuadsBySubjectValue[value]
        return (
          typeQuads !== undefined &&
          quadsHaveIriObject(
            typeQuads,
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://www.w3.org/2002/07/owl#Class",
          )
        )
      })
    ) {
      // Instance of a Class
      dataType = "wikibase-item"
    } else if (
      typeObjects.length === 1 &&
      objectsHaveIri(
        typeObjects,
        "http://www.w3.org/2002/07/owl#NamedIndividual",
      )
    ) {
      // Named Individual without another type
      dataType = "wikibase-item"
    } else {
      throw `Definition of ${subjectValue} has an unknown type:\n${JSON.stringify(
        typeObjects,
        null,
        2,
      )}`
    }
    assert.notStrictEqual(dataType, null)
    dataTypeBySubjectValue[subjectValue] = dataType
  }

  // Generate the set of each URL used as a predicate.
  for (let quads of Object.values(validQuadsBySubjectValue)) {
    for (let quad of quads) {
      predicatesUrls.add(quad.predicate.value)
    }
  }

  // Enumerate the different data types used by each predicate.
  const dataTypesByPredicateUrl = {}
  for (let quads of Object.values(validQuadsBySubjectValue)) {
    for (let quad of quads) {
      const predicateUrl = quad.predicate.value
      let dataTypes = dataTypesByPredicateUrl[predicateUrl]
      if (dataTypes === undefined) {
        dataTypes = dataTypesByPredicateUrl[predicateUrl] = new Set()
      }
      let dataType = dataTypeFromObject(quad.object)
      if (dataType === "boolean") {
        dataType = "wikibase-item"
      }
      dataTypes.add(dataType)
    }
  }

  const request = rpn.defaults({
    headers: {
      "User-Agent": `${pkg.name}/${pkg.version} (${pkg.repository.url}; ${pkg.author})`,
    },
    jar: true,
  })

  const wikibase = new Wikibase({
    ...config.wikibase,
    request,
  })
  await wikibase.login()
  await wikibase.requestCsrfToken()

  // Upsert predicates not defined by FIBO.
  // First ensure that their names are unique.
  const predicateNameByUrl = {}
  const predicateUrlByName = {}
  const equivalentPropertyPropertyId = await getEquivalentPropertyPropertyId(
    wikibase,
  )
  for (let predicateUrl of [...predicatesUrls]) {
    if (validQuadsBySubjectValue[predicateUrl] !== undefined) {
      // Skip predicates defined by FIBO, they will be upserted later.
      continue
    }
    const [predicateSegment0, predicateSegment1] = last2SegmentsFromUrl(
      predicateUrl,
    )
    let predicateName = nameFromSegment(predicateSegment1)
    const existingPredicateUrl = predicateUrlByName[predicateName]
    if (existingPredicateUrl !== undefined) {
      // Another URL has the same name. Change both names.
      const existingPredicateName = `${predicateName} (${nameFromSegment(
        last2SegmentsFromUrl(existingPredicateUrl)[0],
      )})`
      predicateNameByUrl[existingPredicateUrl] = existingPredicateName
      delete predicateUrlByName[predicateName]
      predicateUrlByName[existingPredicateName] = existingPredicateUrl
      predicateName = `${predicateName} (${nameFromSegment(predicateSegment0)})`
    }
    predicateNameByUrl[predicateUrl] = predicateName
    predicateUrlByName[predicateName] = predicateUrl
  }

  // Upsert predicates not defined by FIBO (phase 2).
  for (let [predicateName, predicateUrl] of Object.entries(
    predicateUrlByName,
  )) {
    const dataTypes = dataTypesByPredicateUrl[predicateUrl]
    for (let dataType of [...dataTypes]) {
      const property = await wikibase.upsertPropertyCore({
        datatype: dataType,
        descriptions: [
          {
            language: "en",
            value:
              "predicate used by the Financial Industry Business Ontology (FIBO)",
          },
        ],
        labels: [
          {
            language: "en",
            value:
              dataTypes.size > 1
                ? `${predicateName} (${dataType})`
                : predicateName,
          },
        ],
      })

      const claims = property.claims
      assert(claims !== undefined, JSON.stringify(property, null, 2))
      let lastrevid = property.lastrevid
      assert(lastrevid !== undefined, JSON.stringify(property, null, 2))

      // This property has for equivalent property the URL of the FIBO predicate.
      const existingEquivalentPropertyClaims = (
        claims[equivalentPropertyPropertyId] || []
      ).filter(
        claim =>
          claim.mainsnak.snaktype === "value" &&
          claim.mainsnak.datatype === "url" &&
          claim.mainsnak.datavalue.type === "string" &&
          claim.mainsnak.datavalue.value === predicateUrl,
      )
      if (existingEquivalentPropertyClaims.length === 0) {
        // This property is itself an equivalent property of Wikidata "equivalent property" P1628.
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: property.id,
          format: "json",
          property: equivalentPropertyPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify(predicateUrl),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        assert(result.claim !== undefined, JSON.stringify(result, null, 2))
      }
      console.log(
        "Upserted property: ",
        property.id,
        predicateName,
        dataType,
        predicateUrl,
      )
      let propertyIdByDataType =
        propertyIdByDataTypeByPredicateUrl[predicateUrl]
      if (propertyIdByDataType === undefined) {
        propertyIdByDataType = propertyIdByDataTypeByPredicateUrl[
          predicateUrl
        ] = {}
      }
      propertyIdByDataType[dataType] = property.id
    }
  }

  // Upsert subjects (items and properties) defined by FIBO.
  // First ensure that their names are unique.
  const entityBySubjectValue = {}
  const infosBySubjectValue = {}
  const subjectValueByLabelLowerCaseByLanguage = {}
  for (let [subjectValue, quads] of Object.entries(validQuadsBySubjectValue)) {
    const [subjectSegment0, subjectSegment1] = last2SegmentsFromUrl(
      subjectValue,
    )
    const labelsCandidates = quadsObjects(
      quads,
      "http://www.w3.org/2000/01/rdf-schema#label",
    )
      .map(({ language, value }) => {
        return {
          language: language ? language.split("-")[0] : "en",
          value,
        }
      })
      .filter(({ value }) => value) // Remove empty strings.
    if (labelsCandidates.length === 0) {
      labelsCandidates.push({
        language: "en",
        value: nameFromSegment(subjectSegment1),
      })
    }
    const aliases = []
    const labels = []
    const labelsLanguages = new Set()
    for (let label of labelsCandidates) {
      if (labelsLanguages.has(label.language)) {
        // There is already a label with this language => Use this label as an alias.
        aliases.push(label)
      } else {
        const match = subjectSegment1.match(/\.(\d+)$/)
        labels.push({
          language: label.language,
          value: match === null ? label.value : `${label.value} (${match[1]})`,
        })
        labelsLanguages.add(label.language)
      }
    }

    for (let label of labels) {
      let subjectValueByLabelLowerCase =
        subjectValueByLabelLowerCaseByLanguage[label.language]
      if (subjectValueByLabelLowerCase === undefined) {
        subjectValueByLabelLowerCase = subjectValueByLabelLowerCaseByLanguage[
          label.language
        ] = {}
      }
      let labelValueLowerCase = label.value.toLowerCase()
      const existingSubjectValue =
        subjectValueByLabelLowerCase[labelValueLowerCase]
      if (existingSubjectValue !== undefined) {
        // Another URL has the same name. Change both names.
        const existingSubjectInfos = infosBySubjectValue[existingSubjectValue]
        for (let existingLabel of existingSubjectInfos.labels) {
          const existingLabelValueLowerCase = existingLabel.value.toLowerCase()
          if (existingLabelValueLowerCase === labelValueLowerCase) {
            existingLabel.value = `${existingLabel.value} (${nameFromSegment(
              last2SegmentsFromUrl(existingSubjectValue)[0],
            )})`
            subjectValueByLabelLowerCase[
              existingLabel.value.toLowerCase()
            ] = existingSubjectValue
            delete subjectValueByLabelLowerCase[labelValueLowerCase]
          }
        }
        label.value = `${label.value} (${nameFromSegment(subjectSegment0)})`
        labelValueLowerCase = label.value.toLowerCase()
      } else if (predicateUrlByName[label.value] !== undefined) {
        // A predicate (used by but not defined by FIBO) has the same label. Change label.
        label.value = `${label.value} (${nameFromSegment(subjectSegment0)})`
        labelValueLowerCase = label.value.toLowerCase()
      } else if (reservedEntitiesNames.has(label.value)) {
        // A non-FIBO Wikibase entity has the same label. Change label.
        label.value = `${label.value} (${nameFromSegment(subjectSegment0)})`
        labelValueLowerCase = label.value.toLowerCase()
      }
      subjectValueByLabelLowerCase[labelValueLowerCase] = subjectValue
    }

    const descriptionsCandidates = [
      ...quadsObjects(quads, "http://purl.org/dc/terms/abstract"),
      ...quadsObjects(quads, "http://www.w3.org/2004/02/skos/core#definition"),
    ]
      .map(({ language, value }) => {
        return {
          language: language ? language.split("-")[0] : "en",
          value,
        }
      })
      .filter(({ value }) => value) // Remove empty strings.
    if (descriptionsCandidates.length === 0) {
      descriptionsCandidates.push({
        language: "en",
        value:
          "entity defined by the Financial Industry Business Ontology (FIBO)",
      })
    }
    const descriptions = []
    const descriptionsLanguages = new Set()
    for (let description of descriptionsCandidates) {
      if (!descriptionsLanguages.has(description.language)) {
        descriptions.push(description)
        descriptionsLanguages.add(description.language)
      }
    }

    infosBySubjectValue[subjectValue] = {
      aliases,
      descriptions,
      labels,
    }
  }

  // Upsert subjects (items and properties) defined by FIBO (phase 2).
  for (let [subjectValue, { aliases, descriptions, labels }] of Object.entries(
    infosBySubjectValue,
  )) {
    const dataType = dataTypeBySubjectValue[subjectValue]
    const quads = validQuadsBySubjectValue[subjectValue]
    if (dataType === "wikibase-item") {
      const item = await wikibase.upsertItemCore({
        aliases,
        descriptions,
        labels,
      })

      console.log("Upserted item: ", item.id, subjectValue)
      entityBySubjectValue[subjectValue] = item
    } else if (dataType === "wikibase-property") {
      let dataTypes = dataTypesByPredicateUrl[subjectValue]
      if (dataTypes === undefined) {
        const typeObjects = quadsObjects(
          quads,
          "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
        )
        if (
          objectsHaveIri(
            typeObjects,
            "http://www.w3.org/2002/07/owl#AnnotationProperty",
          )
        ) {
          dataTypes = new Set(["wikibase-item"])
        } else if (
          objectsHaveIri(
            typeObjects,
            "http://www.w3.org/2002/07/owl#DatatypeProperty",
          )
        ) {
          const rangeObjects = quadsObjects(
            quads,
            "http://www.w3.org/2000/01/rdf-schema#range",
          )
          if (rangeObjects.length === 0) {
            // When the range is not defined, suppose that it means URL, hence a wikibase-item.
            dataTypes = new Set(["wikibase-item"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString",
            )
          ) {
            dataTypes = new Set(["monolingualtext"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#anyURI",
            )
          ) {
            dataTypes = new Set(["url"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#boolean",
            )
          ) {
            // Cf https://www.wikidata.org/wiki/Help:Data_type#Boolean: Declined. Suggested alternative: item-datatype.
            dataTypes = new Set(["wikibase-item"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#dateTime",
            )
          ) {
            dataTypes = new Set(["time"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#dateTimeStamp",
            )
          ) {
            dataTypes = new Set(["time"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#decimal",
            )
          ) {
            dataTypes = new Set(["quantity"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#integer",
            )
          ) {
            dataTypes = new Set(["quantity"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#nonNegativeInteger",
            )
          ) {
            dataTypes = new Set(["quantity"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#positiveInteger",
            )
          ) {
            dataTypes = new Set(["quantity"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "http://www.w3.org/2001/XMLSchema#string",
            )
          ) {
            dataTypes = new Set(["string"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "https://spec.edmcouncil.org/fibo/ontology/FND/DatesAndTimes/FinancialDates/CombinedDateTime",
            )
          ) {
            dataTypes = new Set(["time"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "https://spec.edmcouncil.org/fibo/ontology/FND/DatesAndTimes/FinancialDates/dateValue",
            )
          ) {
            dataTypes = new Set(["time"])
          } else if (
            objectsHaveIri(
              rangeObjects,
              "https://spec.edmcouncil.org/fibo/ontology/FND/Places/Countries/Municipality",
            )
          ) {
            dataTypes = new Set(["wikibase-item"])
          } else if (objectsHaveIri(rangeObjects, "b3775")) {
            dataTypes = new Set(["string"])
          } else if (objectsHaveIri(rangeObjects, "b4662")) {
            dataTypes = new Set(["quantity"])
          } else if (objectsHaveIri(rangeObjects, "b4667")) {
            dataTypes = new Set(["quantity"])
          } else {
            throw `Unexpected range for property: ${JSON.stringify(
              quads,
              null,
              2,
            )}`
          }
        } else if (
          objectsHaveIri(
            typeObjects,
            "http://www.w3.org/2002/07/owl#ObjectProperty",
          )
        ) {
          dataTypes = new Set(["wikibase-item"])
        } else {
          throw `Unexpected type for property: ${JSON.stringify(
            quads,
            null,
            2,
          )}`
        }
      }
      for (let dataType of [...dataTypes]) {
        const property = await wikibase.upsertPropertyCore({
          aliases,
          datatype: dataType,
          descriptions,
          labels:
            dataTypes.size > 1
              ? labels.map(label => {
                  return {
                    ...label,
                    value: `${label.value} (${dataType})`,
                  }
                })
              : labels,
        })

        console.log("Upserted property: ", property.id, dataType, subjectValue)
        entityBySubjectValue[subjectValue] = property

        let propertyIdByDataType =
          propertyIdByDataTypeByPredicateUrl[subjectValue]
        if (propertyIdByDataType === undefined) {
          propertyIdByDataType = propertyIdByDataTypeByPredicateUrl[
            subjectValue
          ] = {}
        }
        propertyIdByDataType[dataType] = property.id
      }
    } else {
      throw `Unexpected data type: ${dataType}`
    }
  }

  const equivalentItemPropertyId = await getEquivalentItemPropertyId(wikibase)
  const fiboItemId = await getFiboItemId(wikibase)
  const retrievedPropertyId = await getRetrievedPropertyId(wikibase)
  const statedInPropertyId = await getStatedInPropertyId(wikibase)
  for (let [subjectValue, entity] of Object.entries(entityBySubjectValue)) {
    // console.log(`Upserting claims of entity ${entity.id}`)
    const quads = validQuadsBySubjectValue[subjectValue]

    const claims = entity.claims
    assert.notStrictEqual(claims, undefined, JSON.stringify(entity, null, 2))
    let lastrevid = entity.lastrevid
    assert.notStrictEqual(lastrevid, undefined, JSON.stringify(entity, null, 2))

    if (entity.type === "item") {
      // This item has for equivalent item the FIBO URL.
      const existingEquivalentItemClaims = (
        claims[equivalentItemPropertyId] || []
      ).filter(
        claim =>
          claim.mainsnak.snaktype === "value" &&
          claim.mainsnak.datatype === "url" &&
          claim.mainsnak.datavalue.type === "string" &&
          claim.mainsnak.datavalue.value === subjectValue,
      )
      if (existingEquivalentItemClaims.length === 0) {
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: entity.id,
          format: "json",
          property: equivalentItemPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify(subjectValue),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        assert(result.claim !== undefined, JSON.stringify(result, null, 2))
      }
    } else {
      // Entity is a property.
      // This property has for equivalent property the URL of the FIBO predicate.
      const existingEquivalentPropertyClaims = (
        claims[equivalentPropertyPropertyId] || []
      ).filter(
        claim =>
          claim.mainsnak.snaktype === "value" &&
          claim.mainsnak.datatype === "url" &&
          claim.mainsnak.datavalue.type === "string" &&
          claim.mainsnak.datavalue.value === subjectValue,
      )
      if (existingEquivalentPropertyClaims.length === 0) {
        let result = await wikibase.postToApi({
          action: "wbcreateclaim",
          baserevid: lastrevid,
          bot: true,
          entity: entity.id,
          format: "json",
          property: equivalentPropertyPropertyId,
          snaktype: "value",
          // summary: "Adding claim",
          value: JSON.stringify(subjectValue),
        })
        lastrevid = result.pageinfo.lastrevid
        assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
        assert(result.claim !== undefined, JSON.stringify(result, null, 2))
      }
    }

    // Convert subject statements to Wikibase claims.
    for (let { object, predicate } of quads) {
      const predicateUrl = predicate.value
      if (predicateUrl === "http://www.w3.org/2000/01/rdf-schema#label") {
        continue
      }
      let claim = null
      const objectDataType = dataTypeFromObject(object)
      const simplifiedObjectDataType =
        objectDataType === "boolean" ? "wikibase-item" : objectDataType
      const propertyId =
        propertyIdByDataTypeByPredicateUrl[predicateUrl][
          simplifiedObjectDataType
        ]
      try {
        switch (objectDataType) {
          case "boolean":
            {
              const objectItemId = {
                false: await getFalseItemId(wikibase),
                true: await getTrueItemId(wikibase),
              }[object.value]
              assert.notStrictEqual(
                objectItemId,
                undefined,
                `Ìnvalid value for boolean: ${JSON.stringify(object, null, 2)}`,
              )
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  // && claim.mainsnak.datatype === "wikibase-item"
                  // && claim.mainsnak.datavalue.type === "wikibase-entityid"
                  claim.mainsnak.datavalue.value["entity-type"] === "item" &&
                  claim.mainsnak.datavalue.value.id === objectItemId,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    "entity-type": "item",
                    "numeric-id": numericIdFromItemId(objectItemId),
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          case "monolingualtext":
          case "string": {
            const cleanText = truncate(cleanUpLine(object.value), 400)
            if (!cleanText) {
              console.log(
                "Ignoring empty string",
                entity.id,
                propertyId,
                predicateUrl,
                JSON.stringify(object, null, 2),
              )
              continue
            }
            if (object.language) {
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  claim.mainsnak.datatype === "monolingualtext" &&
                  claim.mainsnak.datavalue.type === "monolingualtext" &&
                  claim.mainsnak.datavalue.value === cleanText,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    language: object.language.split("-")[0],
                    text: cleanText,
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            } else {
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  claim.mainsnak.datatype === "string" &&
                  claim.mainsnak.datavalue.type === "string" &&
                  claim.mainsnak.datavalue.value === cleanText,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify(cleanText),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          }
          case "quantity":
            {
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  claim.mainsnak.datatype === "quantity" &&
                  claim.mainsnak.datavalue.type === "quantity" &&
                  claim.mainsnak.datavalue.value.amount === object.value &&
                  claim.mainsnak.datavalue.value.unit === "1", // = no unit
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    amount: object.value,
                    unit: "1", // = no unit
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          case "time":
            {
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  claim.mainsnak.datatype === "time" &&
                  claim.mainsnak.datavalue.type === "time" &&
                  claim.mainsnak.datavalue.value.precision === 11 && // seconds
                  claim.mainsnak.datavalue.value.time === object.value, // = no unit
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    after: 0,
                    before: 0,
                    calendarmodel: "http://www.wikidata.org/entity/Q1985727",
                    precision: 11, // day
                    time: `+${object.value.split("T")[0]}T00:00:00Z`,
                    timezone: 0,
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          case "url":
            {
              if (
                !object.value.startsWith("http://") &&
                !object.value.startsWith("https://")
              ) {
                console.log(
                  "Ignoring invalid URL",
                  entity.id,
                  propertyId,
                  predicateUrl,
                  JSON.stringify(object, null, 2),
                )
                continue
              }
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  claim.mainsnak.datatype === "url" &&
                  claim.mainsnak.datavalue.type === "string" &&
                  claim.mainsnak.datavalue.value === object.value,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify(object.value),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          case "wikibase-item":
            {
              const objectItem = entityBySubjectValue[object.value]
              assert.notStrictEqual(
                objectItem,
                undefined,
                `Ìnvalid value for wikibase-item: ${JSON.stringify(
                  object,
                  null,
                  2,
                )}`,
              )
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  // && claim.mainsnak.datatype === "wikibase-item"
                  // && claim.mainsnak.datavalue.type === "wikibase-entityid"
                  claim.mainsnak.datavalue.value["entity-type"] === "item" &&
                  claim.mainsnak.datavalue.value.id === objectItem.id,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    "entity-type": "item",
                    "numeric-id": numericIdFromItemId(objectItem.id),
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
          case "wikibase-property":
            {
              const objectItem = entityBySubjectValue[object.value]
              assert.notStrictEqual(
                objectItem,
                undefined,
                `Ìnvalid value for wikibase-property: ${JSON.stringify(
                  object,
                  null,
                  2,
                )}`,
              )
              const existingClaims = (claims[propertyId] || []).filter(
                claim =>
                  claim.mainsnak.snaktype === "value" &&
                  // && claim.mainsnak.datatype === "wikibase-property"
                  // && claim.mainsnak.datavalue.type === "wikibase-entityid"
                  claim.mainsnak.datavalue.value["entity-type"] ===
                    "property" &&
                  claim.mainsnak.datavalue.value.id === objectItem.id,
              )
              if (existingClaims.length === 0) {
                let result = await wikibase.postToApi({
                  action: "wbcreateclaim",
                  baserevid: lastrevid,
                  bot: true,
                  entity: entity.id,
                  format: "json",
                  property: propertyId,
                  snaktype: "value",
                  // summary: "Adding claim",
                  value: JSON.stringify({
                    "entity-type": "property",
                    "numeric-id": numericIdFromItemId(objectItem.id),
                  }),
                })
                lastrevid = result.pageinfo.lastrevid
                assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
                claim = result.claim
                assert(claim !== undefined, JSON.stringify(result, null, 2))
              } else {
                claim = existingClaims[0]
              }
            }
            break
        }
        const existingReferences = (claims.references || []).filter(
          reference => {
            const statedInSnaks = reference.snaks[statedInPropertyId]
            if (statedInSnaks === undefined || statedInSnaks.length !== 1) {
              return false
            }
            const statedInSnak = statedInSnaks[0]
            return (
              statedInSnak.snaktype === "value" &&
              // && statedInSnak.datatype === "wikibase-item"
              // && statedInSnak.datavalue.type === "wikibase-entityid"
              statedInSnak.datavalue.value["entity-type"] === "item" &&
              statedInSnak.datavalue.value.id === fiboItemId
            )
          },
        )
        {
          const formBody = {
            action: "wbsetreference",
            baserevid: lastrevid,
            bot: true,
            format: "json",
            snaks: JSON.stringify({
              [retrievedPropertyId]: [
                {
                  property: retrievedPropertyId,
                  snaktype: "value",
                  datavalue: {
                    type: "time",
                    value: {
                      after: 0,
                      before: 0,
                      calendarmodel: "http://www.wikidata.org/entity/Q1985727",
                      precision: 11, // day
                      time: `+${
                        quadsModificationTime.toISOString().split("T")[0]
                      }T00:00:00Z`,
                      timezone: 0,
                    },
                  },
                },
              ],
              [statedInPropertyId]: [
                {
                  property: statedInPropertyId,
                  snaktype: "value",
                  datavalue: {
                    type: "wikibase-entityid",
                    value: {
                      "entity-type": "item",
                      "numeric-id": numericIdFromItemId(fiboItemId),
                    },
                  },
                },
              ],
            }),
            "snaks-order": JSON.stringify([
              retrievedPropertyId,
              statedInPropertyId,
            ]),
            statement: claim.id,
            // summary: "Upserting reference",
          }
          if (existingReferences.length > 0) {
            formBody.reference = existingReferences[0].hash
          }
          const result = await wikibase.postToApi(formBody)
          lastrevid = result.pageinfo.lastrevid
          assert(lastrevid !== undefined, JSON.stringify(result, null, 2))
          assert(
            result.reference !== undefined,
            JSON.stringify(result, null, 2),
          )
        }
      } catch (e) {
        console.log(
          entity.id,
          propertyId,
          predicateUrl,
          JSON.stringify(object, null, 2),
        )
        throw e
      }
    }
  }
}

function nameFromSegment(segment) {
  let name = ""
  let previousLowerCase = false
  for (let c of segment) {
    const isUpperCase = c.toUpperCase() === c
    if (isUpperCase && previousLowerCase) {
      name += " "
    }
    if (c === "_" || c === "-") {
      name += " "
    } else {
      name += c
    }
    previousLowerCase = !isUpperCase
  }
  return name
    .replace(/ {2,}/g, " ")
    .toLowerCase()
    .trim()
}

function objectsHaveIri(objects, objectValue) {
  return objects.some(
    object => object.value === objectValue && object.datatype === undefined,
  )
}

function quadsHaveIriObject(quads, predicateValue, objectValue) {
  return quads.some(
    ({ predicate, object }) =>
      predicate.value === predicateValue &&
      object.value === objectValue &&
      object.datatype === undefined,
  )
}

function quadsHavePredicate(quads, predicateValue) {
  return quads.some(({ predicate }) => predicate.value === predicateValue)
}

function quadsObjects(quads, predicateValue) {
  return quads
    .filter(({ predicate }) => predicate.value === predicateValue)
    .map(({ object }) => object)
}

main().catch(error => {
  console.error(error)
  process.exit(1)
})
