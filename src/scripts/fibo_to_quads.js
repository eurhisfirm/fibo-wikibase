import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs"
import fetch from "node-fetch"
import path from "path"
import { RdfXmlParser } from "rdfxml-streaming-parser"

import config from "../config"

const quadsBySubjectValue = {}
const fiboUrlsToFetch = []
const fiboUrls = new Set()
const optionDefinitions = [
  { name: "dir", type: String, defaultOption: true, help: "directory where to store FIBO quads" },
]
const options = commandLineArgs(optionDefinitions)

async function fetchQuads(url) {
  const response = await fetch(url)
  assert(response.ok, `Fetch of ${url} failed.`)
  return await new Promise(resolve => {
    const quads = []
    const rdfXmlParser = new RdfXmlParser()
    rdfXmlParser
      .import(response.body)
      .on("data", quad => quads.push(quad))
      .on("end", () => resolve(quads))
      .on("error", console.error)
  })
}

async function fetchFiboUrl(fiboUrl) {
  console.log(`Fetching ${fiboUrl}`)
  const quads = await fetchQuads(fiboUrl)
  quads.forEach((quad) => {
    const { object, predicate, subject } = quad
    if (predicate.value === "http://www.w3.org/2002/07/owl#imports") {
      const fiboUrl = object.value
      if (!fiboUrls.has(fiboUrl)) {
        fiboUrls.add(fiboUrl)
        fiboUrlsToFetch.push(fiboUrl)
      }
    } else {
      const subjectValue = subject.value
      let subjectQuads = quadsBySubjectValue[subjectValue]
      if (subjectQuads === undefined) {
        subjectQuads = quadsBySubjectValue[subjectValue] = []
      }
      subjectQuads.push(quad)
    }
  })
}

async function main() {
  const fiboUrl = config.fiboRdfXmlUrl
  if (!fiboUrls.has(fiboUrl)) {
    fiboUrls.add(fiboUrl)
    fiboUrlsToFetch.push(fiboUrl)
  }
  while(fiboUrlsToFetch.length > 0) {
    await fetchFiboUrl(fiboUrlsToFetch.shift())
  }
  assert(fs.statSync(options.dir).isDirectory())
  fs.writeFileSync(path.join(options.dir, "fibo_quads.json"), JSON.stringify(quadsBySubjectValue, null, 2))
}

main()
  .catch(error => {
    console.error(error)
    process.exit(1)
  })