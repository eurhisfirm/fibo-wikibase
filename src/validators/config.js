import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core"

export function validateConfig(config) {
  if (config === null || config === undefined) {
    return [config, "Missing config"]
  }
  if (typeof config !== "object") {
    return [config, `Expected an object got "${typeof config}"`]
  }

  config = { ...config }
  const errors = {}
  const remainingKeys = new Set(Object.keys(config))

  {
    const key = "fiboRdfXmlUrl"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateUrl(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "wikibase"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateWikibase(config[key])
      config[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [config, Object.keys(errors).length === 0 ? null : errors]
}

function validateWikibase(wikibase) {
  if (wikibase === null || wikibase === undefined) {
    return [wikibase, "Missing value"]
  }
  if (typeof wikibase !== "object") {
    return [wikibase, `Expected an object got "${typeof wikibase}"`]
  }

  wikibase = { ...wikibase }
  const errors = {}
  const remainingKeys = new Set(Object.keys(wikibase))

  for (let key of ["password", "site", "user"]) {
    if (remainingKeys.delete(key)) {
      const [value, error] = validateNonEmptyTrimmedString(wikibase[key])
      wikibase[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  {
    const key = "url"
    if (remainingKeys.delete(key)) {
      const [value, error] = validateUrl(wikibase[key])
      wikibase[key] = value
      if (error !== null) {
        errors[key] = error
      }
    } else {
      errors[key] = "Missing item"
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [wikibase, Object.keys(errors).length === 0 ? null : errors]
}
