import {
  validateChain,
  validateChoice,
  validateFunction,
  validateMissing,
  validateOption,
  validateStrictEqual,
  validateString,
  validateTest,
  validateUrl,
} from "@biryani/core"

const validateAndRepairUrl = validateChain(
  validateUrl,
  // Correct bug in some OMG quads.
  validateFunction(url =>
    url
      .replace(/http:\/\/www\.omg\.org\/.+\/&dct;/, "http://purl.org/dc/terms/")
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-3166-1;/,
        "http://www.omg.org/spec/LCC/Countries/ISO3166-1-CountryCodes/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-3166-2;/,
        "http://www.omg.org/spec/LCC/Countries/ISO3166-2-SubdivisionCodes/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-3166-2-([a-z]{2});/,
        (match, p1) =>
          `http://www.omg.org/spec/LCC/Countries/Regions/ISO3166-2-SubdivisionCodes-${p1.toUpperCase()}/`,
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-639-1;/,
        "http://www.omg.org/spec/LCC/Languages/ISO639-1-LanguageCodes/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-639-2;/,
        "http://www.omg.org/spec/LCC/Languages/ISO639-2-LanguageCodes/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-cr;/,
        "http://www.omg.org/spec/LCC/Countries/CountryRepresentation/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-lr;/,
        "http://www.omg.org/spec/LCC/Languages/LanguageRepresentation/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&lcc-m49;/,
        "http://www.omg.org/spec/LCC/Countries/UN-M49-RegionCodes/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&skos;/,
        "http://www.w3.org/2004/02/skos/core#",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&sm;/,
        "http://www.omg.org/techprocess/ab/SpecificationMetadata/",
      )
      .replace(
        /http:\/\/www\.omg\.org\/.+\/&xsd;/,
        "http://www.w3.org/2001/XMLSchema#",
      ),
  ),
  validateTest(url => !url.includes("&")),
)

export const validateSubjectValue = validateOption([
  validateAndRepairUrl,
  [validateString, validateTest(value => /b\d+$/.test(value), "Invalid value")],
])

function validateDataType(dataType) {
  if (dataType === null || dataType === undefined) {
    return [dataType, "Missing data type"]
  }
  if (typeof dataType !== "object") {
    return [dataType, `Expected an object got "${typeof dataType}"`]
  }

  dataType = { ...dataType }
  const errors = {}
  const remainingKeys = new Set(Object.keys(dataType))

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateChain(
      validateAndRepairUrl,
      validateChoice([
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString",
        "http://www.w3.org/2001/XMLSchema#anyURI",
        "http://www.w3.org/2001/XMLSchema#boolean",
        "http://www.w3.org/2001/XMLSchema#dateTime",
        "http://www.w3.org/2001/XMLSchema#decimal",
        "http://www.w3.org/2001/XMLSchema#nonNegativeInteger",
        "http://www.w3.org/2001/XMLSchema#string",
      ]),
    )(dataType[key])
    dataType[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [dataType, Object.keys(errors).length === 0 ? null : errors]
}

function validateGraph(graph) {
  if (graph === null || graph === undefined) {
    return [graph, "Missing graph"]
  }
  if (typeof graph !== "object") {
    return [graph, `Expected an object got "${typeof graph}"`]
  }

  graph = { ...graph }
  const errors = {}
  const remainingKeys = new Set(Object.keys(graph))

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateStrictEqual("")(graph[key])
    graph[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [graph, Object.keys(errors).length === 0 ? null : errors]
}

function validateObject(object) {
  if (object === null || object === undefined) {
    return [object, "Missing object"]
  }
  if (typeof object !== "object") {
    return [object, `Expected an object got "${typeof object}"`]
  }

  object = { ...object }
  const errors = {}
  const remainingKeys = new Set(Object.keys(object))

  {
    const key = "datatype"
    remainingKeys.delete(key)
    const [value, error] = validateOption([validateMissing, validateDataType])(
      object[key],
    )
    if (value === null) {
      delete object[key]
    } else {
      object[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "language"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      validateMissing,
      [validateString, validateChoice(["", "en", "en-gb", "en-us", "fr"])],
    ])(object[key])
    if (value === null) {
      delete object[key]
    } else {
      object[key] = value
    }
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateOption([
      // When object value is an URL, repair it.
      validateAndRepairUrl,
      // Else ensure, that it is a string.
      validateString,
    ])(object[key])
    object[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }

  if (Object.keys(errors).length > 0) {
    return [object, errors]
  }

  const dataTypeUrl =
    object.datatype === undefined ? null : object.datatype.value
  switch (dataTypeUrl) {
    case "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString":
      if (object.language === undefined) {
        errors["language"] = "Missing value"
      } else if (object.language === "") {
        errors["language"] = "Expected a non empty string"
      }
      break
    default:
      if (object.language !== undefined && object.language !== "") {
        errors["language"] = "Unexpected value"
      }
      break
  }

  return [object, Object.keys(errors).length === 0 ? null : errors]
}

function validatePredicate(predicate) {
  if (predicate === null || predicate === undefined) {
    return [predicate, "Missing predicate"]
  }
  if (typeof predicate !== "object") {
    return [predicate, `Expected an object got "${typeof predicate}"`]
  }

  predicate = { ...predicate }
  const errors = {}
  const remainingKeys = new Set(Object.keys(predicate))

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateAndRepairUrl(predicate[key])
    predicate[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [predicate, Object.keys(errors).length === 0 ? null : errors]
}

export function validateQuad(quad) {
  if (quad === null || quad === undefined) {
    return [quad, "Missing quad"]
  }
  if (typeof quad !== "object") {
    return [quad, `Expected an object got "${typeof quad}"`]
  }

  quad = { ...quad }
  const errors = {}
  const remainingKeys = new Set(Object.keys(quad))

  {
    const key = "graph"
    remainingKeys.delete(key)
    const [value, error] = validateGraph(quad[key])
    quad[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "object"
    remainingKeys.delete(key)
    const [value, error] = validateObject(quad[key])
    quad[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "predicate"
    remainingKeys.delete(key)
    const [value, error] = validatePredicate(quad[key])
    quad[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "subject"
    remainingKeys.delete(key)
    const [value, error] = validateSubject(quad[key])
    quad[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [quad, Object.keys(errors).length === 0 ? null : errors]
}

function validateSubject(subject) {
  if (subject === null || subject === undefined) {
    return [subject, "Missing subject"]
  }
  if (typeof subject !== "object") {
    return [subject, `Expected an object got "${typeof subject}"`]
  }

  subject = { ...subject }
  const errors = {}
  const remainingKeys = new Set(Object.keys(subject))

  {
    const key = "value"
    remainingKeys.delete(key)
    const [value, error] = validateSubjectValue(subject[key])
    subject[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected entry"
  }
  return [subject, Object.keys(errors).length === 0 ? null : errors]
}
